﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportCardManagementApp.Models;

namespace ReportCardManagementApp.ViewModels
{
    public class StudentClassesIndexViewModel
    {
        public List<StudentEnrollmentClasses> Counts { get; set; }
        
        public class StudentEnrollmentClasses
        {
            public int ClassID { get; set; }
            public string ClassName { get; set; }
            public int NumberOfEnrollments { get; set; }
        }
    }
}