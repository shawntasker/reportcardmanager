﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportCardManagementApp.Models;

namespace ReportCardManagementApp.ViewModels
{
    public class TeacherDashboardIndexViewModel
    {
        public int NeedsGrading { get; set; }
        public int Assigned { get; set; }
        public int Graded { get; set; }
        public int Enrollment { get; set; }
        public int Assignments { get; set; }
        public int ReportCards { get; set; }
    }
}