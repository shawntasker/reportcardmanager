﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportCardManagementApp.Models;

namespace ReportCardManagementApp.ViewModels
{
    public class StudentClassesEnrollmentListViewModel
    {
        public List<EnrollmentList> Students { get; set; }
        public Class Class { get; set; }

        public class EnrollmentList
        {
            public string StudentName { get; set; }
        }
    }
}