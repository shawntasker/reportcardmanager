﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportCardManagementApp.Models;

namespace ReportCardManagementApp.ViewModels
{
    public class GenerateReportCardIndexViewModel
    {
        public List<AverageGradeClasses> Grades { get; set; }

        public class AverageGradeClasses
        {
            public string ClassName { get; set; }
            public string Grade { get; set; }
        }
    }
}