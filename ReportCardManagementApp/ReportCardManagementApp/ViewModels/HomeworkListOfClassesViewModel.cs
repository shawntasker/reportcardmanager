﻿using ReportCardManagementApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportCardManagementApp.ViewModels
{
    public class HomeworkListOfClassesViewModel
    {
        public List<ClassWithFilteredHomework> Classes { get; set; }

        public class ClassWithFilteredHomework
        {
            public Class InClass { get; set; }
            public int Homework { get; set; }
        }
    }
}