﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReportCardManagementApp.Models;
using ReportCardManagementApp.ViewModels;

namespace ReportCardManagementApp.Controllers
{
    public class GenerateReportCardController : Controller
    {
        ReportCardDBEntities db = new ReportCardDBEntities();

        // GET: GenerateReportCard
        public ActionResult Index()
        {
            var students = db.Students;
            return View(students);
        }

        

        public ActionResult Generate(int studentID)
        {
            var classes = db.StudentClasses.Where(x => x.StudentID == studentID);
            List<double?> homeworks = new List<double?>();
            GenerateReportCardIndexViewModel model;
            model = new GenerateReportCardIndexViewModel();
            

            foreach (StudentClass c in classes) // for each class the student is enrolled in
            {
                GenerateReportCardIndexViewModel.AverageGradeClasses agc;
                model.Grades = new List<GenerateReportCardIndexViewModel.AverageGradeClasses>();

                foreach (Homework hw in c.Student.Homework)
                {
                    homeworks.Add(hw.Grade);
                }

                agc = new GenerateReportCardIndexViewModel.AverageGradeClasses();
                agc.ClassName = c.Class.ClassName;
                agc.Grade = LetterGrade(homeworks.Average());

                if(agc != null)
                {
                    model.Grades.Add(agc);
                }
                
                homeworks.Clear();
            }

            Student s = db.Students.Find(studentID);
            ViewBag.Student = s;

            return View(model);
        }

        public string LetterGrade(double? grade)
        {
            if(grade > 89)
            {
                return "A";
            }
            else if(grade > 79)
            {
                return "B";
            }
            else if(grade > 69)
            {
                return "C";
            }
            else if(grade > 59)
            {
                return "D";
            }
            return "F";
        }

        public ActionResult MassEnrollment()
        {
            var students = db.Students;
            var classes = db.Classes.ToArray();
            Random random = new Random();
            int classIndex = 0;

            if (ModelState.IsValid)
            {
                foreach (Student s in students.ToList())
                {
                    classIndex = random.Next(classes.Count());
                    StudentClass sc = new StudentClass()
                    {
                        StudentID = s.StudentID,
                        ClassID = classes[classIndex].ClassID,
                        DateFrom = DateTime.Now,
                        DateTo = DateTime.Now.Add(new TimeSpan(180, 0, 0, 0))
                    };

                    db.StudentClasses.Add(sc);
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MassAssignments()
        {
            var students = db.Students;
            var classes = db.Classes;
            Random random = new Random();

            if (ModelState.IsValid)
            {
                foreach (Class @class in classes)
                {
                    List<StudentClass> enrolledStudents = db.StudentClasses.Where(sc => sc.ClassID == @class.ClassID).ToList();

                    foreach(StudentClass sc in enrolledStudents)
                    {
                        for (int i = 0; i < 15; i++) // create 15 assignments per student with a random grade
                        {
                            Homework homework = new Homework();
                            homework.StudentID = sc.StudentID;
                            homework.ClassID = sc.ClassID;
                            homework.DateCreated = DateTime.Now;
                            homework.Grade = random.Next(68, 100);
                            db.Homework.Add(homework);
                            db.SaveChanges();
                        }
                    }
                }
                return RedirectToAction("Index");
            }
            return View();
        }

    }
}