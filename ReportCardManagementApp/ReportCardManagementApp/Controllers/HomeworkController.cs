﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ReportCardManagementApp.Models;
using ReportCardManagementApp.ViewModels;

namespace ReportCardManagementApp.Controllers
{
    public class HomeworkController : Controller
    {
        private ReportCardDBEntities db = new ReportCardDBEntities();

        // GET: Homework
        public ActionResult Index(string filter)
        {
            return View(FilterHomework(filter));
        }

        public List<Homework> FilterHomework(string filter)
        {
            List<Homework> hwList = new List<Homework>();
            var homework = db.Homework.Include(h => h.Class).Include(h => h.Student);
            
            if(!string.IsNullOrEmpty(filter))
            {
                if (filter.Equals("Assigned"))
                {
                    foreach (Homework hw in homework)
                    {
                        if (hw.Grade == 0)
                        {
                            hwList.Add(hw);
                        }
                    }
                    ViewBag.Filter = "Assigned";
                    return hwList;
                }
                else if (filter.Equals("Turned In"))
                {
                    foreach (Homework hw in homework)
                    {
                        if (hw.Grade == 1)
                        {
                            hwList.Add(hw);
                        }
                    }
                    ViewBag.Filter = "Turned In";
                    return hwList;
                }
                else if (filter.Equals("Graded"))
                {
                    foreach (Homework hw in homework)
                    {
                        if (hw.Grade > 1)
                        {
                            hwList.Add(hw);
                        }
                    }
                    ViewBag.Filter = "Graded";
                    return hwList;
                }
                else if(filter.Equals("All"))
                {
                    foreach(Homework hw in homework)
                    {
                        hwList.Add(hw);
                    }
                }
            }

            ViewBag.Filter = "All";
            return homework.ToList();

        }

        public ActionResult ListOfClasses(string filter)
        {
            var classes = db.Classes;
            HomeworkListOfClassesViewModel model = new HomeworkListOfClassesViewModel();
            model.Classes = new List<HomeworkListOfClassesViewModel.ClassWithFilteredHomework>();

            if(string.IsNullOrEmpty(filter))
            {
                filter = "All";
            }

            foreach(Class InClass in classes)
            {
                HomeworkListOfClassesViewModel.ClassWithFilteredHomework cwfh = new HomeworkListOfClassesViewModel.ClassWithFilteredHomework();
                cwfh.InClass = InClass;
                cwfh.Homework = FilterHomework(filter).Where(x => x.ClassID == InClass.ClassID).Count();
                model.Classes.Add(cwfh);
            }

            return View(model);
        }

        public ActionResult ListOfClassHomework(int? id, string filter)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<Homework> homeworks = FilterHomework(filter);
            ViewBag.Filter = filter;
            return View(homeworks);
        }

        // GET: Homework/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Homework homework = await db.Homework.FindAsync(id);
            if (homework == null)
            {
                return HttpNotFound();
            }
            return View(homework);
        }

        // GET: Homework/Create
        public ActionResult Create()
        {
            ViewBag.ClassID = new SelectList(db.Classes, "ClassID", "ClassCode");
            ViewBag.StudentID = new SelectList(db.Students, "StudentID", "FirstName");
            return View();
        }

        // POST: Homework/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "HomeworkID,StudentID,DateCreated,HomeWorkContent,Grade,OtherHomeworkDetails,ClassID")] Homework homework)
        {
            if (ModelState.IsValid)
            {
                List<StudentClass> enrolledStudents = db.StudentClasses.Where(sc => sc.ClassID == homework.ClassID).ToList();

                foreach(StudentClass sc in enrolledStudents)
                {
                    homework.StudentID = sc.StudentID;
                    db.Homework.Add(homework);
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }

            ViewBag.ClassID = new SelectList(db.Classes, "ClassID", "ClassCode", homework.ClassID);
            ViewBag.StudentID = new SelectList(db.Students, "StudentID", "FirstName", homework.StudentID);
            return View(homework);
        }

        // GET: Homework/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Homework homework = await db.Homework.FindAsync(id);
            if (homework == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClassID = new SelectList(db.Classes, "ClassID", "ClassCode", homework.ClassID);
            ViewBag.StudentID = new SelectList(db.Students, "StudentID", "FirstName", homework.StudentID);
            return View(homework);
        }

        // POST: Homework/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "HomeworkID,StudentID,DateCreated,HomeWorkContent,Grade,OtherHomeworkDetails,ClassID")] Homework homework)
        {
            if (ModelState.IsValid)
            {
                db.Entry(homework).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.ClassID = new SelectList(db.Classes, "ClassID", "ClassCode", homework.ClassID);
            ViewBag.StudentID = new SelectList(db.Students, "StudentID", "FirstName", homework.StudentID);
            return View(homework);
        }

        // GET: Homework/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Homework homework = await db.Homework.FindAsync(id);
            if (homework == null)
            {
                return HttpNotFound();
            }
            return View(homework);
        }

        // POST: Homework/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Homework homework = await db.Homework.FindAsync(id);
            db.Homework.Remove(homework);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
