﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ReportCardManagementApp.ViewModels;
using ReportCardManagementApp.Models;
using System.Threading.Tasks;

namespace ReportCardManagementApp.Controllers
{
    public class TeacherDashboardController : Controller
    {
        ReportCardDBEntities db = new ReportCardDBEntities();
        // GET: TeacherDashboard
        public ActionResult Index() // could i use "await" to make this controller's methods faster?
        {
            TeacherDashboardIndexViewModel model = new TeacherDashboardIndexViewModel();

            var homeworks = db.Homework;

            model.Enrollment = db.StudentClasses.Count();
            model.Assignments = homeworks.Count();
            model.ReportCards = db.Students.Count();


            foreach(Homework hw in homeworks)
            {
                if (hw.Grade == 1)
                {
                    model.NeedsGrading++;
                }
                else if (hw.Grade == 0)
                {
                    model.Assigned++;
                }
                else
                {
                    model.Graded++;
                }
            }

            return View(model);
        }

        public ActionResult Charts()
        {
            return View();
        }

        public ActionResult Tables()
        {
            return View();
        }

        public ActionResult Forms()
        {
            return View();
        }

        public ActionResult BootstrapElements()
        {
            return View();
        }

        public ActionResult BootstrapGrid()
        {
            return View();
        }
    }
}