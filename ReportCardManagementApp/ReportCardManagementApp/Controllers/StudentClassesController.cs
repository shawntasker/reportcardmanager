﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ReportCardManagementApp.Models;
using ReportCardManagementApp.ViewModels;

namespace ReportCardManagementApp.Controllers
{
    public class StudentClassesController : Controller
    {
        private ReportCardDBEntities db = new ReportCardDBEntities();

        // GET: StudentClasses
        public async Task<ActionResult> Index()
        {
            var studentClasses = db.StudentClasses.Include(s => s.Class).Include(s => s.Student);

            StudentClassesIndexViewModel model = new StudentClassesIndexViewModel();

            var listOfsec = db.Classes.Select(x => new StudentClassesIndexViewModel.StudentEnrollmentClasses()
            {
                ClassID = x.ClassID,
                ClassName = x.ClassName,
                NumberOfEnrollments = x.StudentClasses.Count
            });
            model.Counts = await listOfsec.ToListAsync();
            return View(model);
        }

        public async Task<ActionResult> EnrollmentList(int id)
        {
            Class inclass = await db.Classes.FindAsync(id);

            var listOfEnrolled = db.StudentClasses.Where(x => x.ClassID == inclass.ClassID);

            List<StudentClass> enrolled = await listOfEnrolled.ToListAsync();

            ViewBag.ClassName = inclass.ClassName;

            return View(enrolled);
        }

        // GET: StudentClasses/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StudentClass studentClass = await db.StudentClasses.FindAsync(id);
            if (studentClass == null)
            {
                return HttpNotFound();
            }
            return View(studentClass);
        }

        // GET: StudentClasses/Create
        public ActionResult Create()
        {
            ViewBag.ClassID = new SelectList(db.Classes, "ClassID", "ClassCode");
            ViewBag.StudentID = new SelectList(db.Students, "StudentID", "FirstName");
            return View();
        }

        // POST: StudentClasses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "StudentID,ClassID,DateFrom,DateTo")] StudentClass studentClass)
        {
            if (ModelState.IsValid)
            {
                db.StudentClasses.Add(studentClass);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.ClassID = new SelectList(db.Classes, "ClassID", "ClassCode", studentClass.ClassID);
            ViewBag.StudentID = new SelectList(db.Students, "StudentID", "FirstName", studentClass.StudentID);
            return View(studentClass);
        }

        // GET: StudentClasses/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StudentClass studentClass = await db.StudentClasses.FindAsync(id);
            if (studentClass == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClassID = new SelectList(db.Classes, "ClassID", "ClassCode", studentClass.ClassID);
            ViewBag.StudentID = new SelectList(db.Students, "StudentID", "FirstName", studentClass.StudentID);
            return View(studentClass);
        }

        // POST: StudentClasses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "StudentID,ClassID,DateFrom,DateTo")] StudentClass studentClass)
        {
            if (ModelState.IsValid)
            {
                db.Entry(studentClass).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.ClassID = new SelectList(db.Classes, "ClassID", "ClassCode", studentClass.ClassID);
            ViewBag.StudentID = new SelectList(db.Students, "StudentID", "FirstName", studentClass.StudentID);
            return View(studentClass);
        }

        // GET: StudentClasses/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StudentClass studentClass = await db.StudentClasses.FindAsync(id);
            if (studentClass == null)
            {
                return HttpNotFound();
            }
            return View(studentClass);
        }

        // POST: StudentClasses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            StudentClass studentClass = await db.StudentClasses.FindAsync(id);
            db.StudentClasses.Remove(studentClass);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
