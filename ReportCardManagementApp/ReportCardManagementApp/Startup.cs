﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ReportCardManagementApp.Startup))]
namespace ReportCardManagementApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
